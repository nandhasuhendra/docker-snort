﻿FROM ubuntu:16.04

MAINTAINER Nandha Suhendra <nandhasuhendra@gmail.com>

ENV DAQ_VERSION 2.0.6
ENV SNORT_VERSION 2.9.11.1
ENV NETWORK_INTERFACE enp2s0

RUN apt-get update && \
    apt-get install -y \
        openssh-server \
        ethtool \
        build-essential \
        libpcap-dev \
        libpcre3-dev \
        libdumbnet-dev \
        bison \
        flex \
        zlib1g-dev \
        liblzma-dev \
        openssl \
        libssl-dev \
        iptables-dev \
        wget \
        vim \
        tcpdump \
        unzip \
        iputils-ping \
        net-tools

WORKDIR /opt

RUN wget https://www.snort.org/downloads/snort/daq-${DAQ_VERSION}.tar.gz \
    && tar -zxvf daq-${DAQ_VERSION}.tar.gz \
    && cd daq-${DAQ_VERSION} \
    && ./configure \
    && make \
    && make install

RUN wget https://www.snort.org/downloads/snort/snort-${SNORT_VERSION}.tar.gz \
    && tar -xvzf snort-${SNORT_VERSION}.tar.gz \
    && cd snort-${SNORT_VERSION} \
    && ./configure --enable-sourcefire \
    && make \
    && make install

RUN ldconfig
RUN ln -s /usr/local/bin/snort /usr/sbin/snort

RUN mkdir /etc/snort \
    && mkdir /etc/snort/prepoc_rules \
    && mkdir /etc/snort/rules \
    && mkdir /var/log/snort \
    && mkdir /usr/local/lib/snort_dynamicrules \
    && touch /etc/snort/rules/white_list.rules /etc/snort/rules/black_list.rules /etc/snort/rules/local.rules

RUN chmod -R 5775 /etc/snort/ \
    && chmod -R 5775 /var/log/snort/ \
    && chmod -R 5775 /usr/local/lib/snort \
    && chmod -R 5775 /usr/local/lib/snort_dynamicrules/

RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    /opt/snort-${SNORT_VERSION}.tar.gz /opt/daq-${DAQ_VERSION}.tar.gz

RUN cd snort-${SNORT_VERSION} \
    && cp -avr etc/* /etc/snort/ \
    && cp -avr preproc_rules/* /etc/snort/prepoc_rules/ \
    && cp -avr src/dynamic-preprocessors/build/usr/local/lib/snort_dynamicpreprocessor/* /usr/local/lib/snort_dynamicpreprocessor/
